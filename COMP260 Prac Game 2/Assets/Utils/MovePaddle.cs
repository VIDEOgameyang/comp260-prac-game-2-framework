﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour
{
    private Rigidbody rigidbody;

    public string horizontalAxis;
    public string verticalAxis;

    public float speed = 20f;
    public float force = 10f;

    // Use this for initialization

    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }


    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;

    }

    // Update is called once per frame
    void Update()
    {

        Debug.Log("Time =" + Time.time);
    }

    void FixedUpdate()
    {
        Vector3 direction = rigidbody.position;
        direction.x = Input.GetAxis(horizontalAxis);
        direction.z = Input.GetAxis(verticalAxis);

        //Debug.Log("direction.x = " + direction.x);
        //Debug.Log("direction.z = " + direction.z);

        direction = direction * speed;

        rigidbody.velocity = direction;
    }
}
