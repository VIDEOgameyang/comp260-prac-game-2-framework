﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour
{
    private Rigidbody rigidbody;

    //public Transform paddlePos;
    public Transform puckPos;

    //public string horizontalAxis;
    //public string verticalAxis;

    public float speed;
    //public float acceleration;
    //public float brake;

    //public float maxSpeed;
    //public float force;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Time = " + Time.time);
    }

    void FixedUpdate()
    {
        Vector3 direction = rigidbody.position;
        //direction.x = Input.GetAxis(horizontalAxis);
        //direction.z = Input.GetAxis(verticalAxis);

        //Debug.Log("direction.x = " + direction.x);
        //Debug.Log("direction.z = " + direction.z);

        if (direction.x - puckPos.position.x > 0)
        {
            direction.x = -1;
        }
        else if (direction.x - puckPos.position.x < 0)
        {
            direction.x = 1;
        }
        else
        {
            direction.x = 0;
        }

        if (direction.z - puckPos.position.z > 0)
        {
            direction.z = -1;
        }
        else if (direction.z - puckPos.position.z < 0)
        {
            direction.z = 1;
        }
        else
        {
            direction.z = 0;
        }

        direction = direction * speed;

        rigidbody.velocity = direction;
    }

}
